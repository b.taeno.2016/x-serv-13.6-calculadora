def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


def calculadora():
    print("Resultado de sumar 1 y 2: " + str(sumar(1, 2)))
    print("Resultado de sumar 3 y 4: " + str(sumar(3, 4)))
    print("Resultado de restar 5 a 6: " + str(restar(6, 5)))
    print("Resultado de restar 7 a 8: " + str(restar(8, 7)))


calculadora()
